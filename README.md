# Description of the architecture

The project consists of 3 modules `Domain`, `Data`, and
`Presentation`.

## `Domain` module

Contains Entities, Use cases & Repository Interfaces. 
Use cases can combine data from 1 or multiple Repository Interfaces.
Domain Layer depends on Data Layer.


## `Data` module

Contains Repository Implementations and 1 or multiple Data Sources. 
Repositories are responsible to coordinate data from the different Data Sources. 


## `Presentation` module

Contains UI (Activities & Fragments) that are coordinated by ViewModels 
which execute 1 or multiple Use cases. 
Presentation Layer depends on Domain Layer.


# Explaining Concepts

## `Use cases`

This is where we interact with our repositories to do very specific tasks, generally asynchronously. 


## `ViewModel`

This makes use of the new architecture components so that we don’t need to know about our view, 
which you may traditionally need to know about if you was using a presenter. 
This has the added benefit of allowing multiple views to connect to a single view model. 
This layer may contain several use cases that can be executed to do specific tasks. 
This makes use of a live data which allows various sources from our use-cases to be added 
as sources which will all be propagated the live data’s active/inactive state. 
This means our view only needs to observe one live data rather than one for each use-case. 
This live data will expose a state to our view to keep things neat and simple.


## `View`

This layer comprises of our platform specific view, i.e. a fragment or an activity. 