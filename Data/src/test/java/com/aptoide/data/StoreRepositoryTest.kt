package com.aptoide.data

import com.aptoide.data.entity.AppEntity
import com.aptoide.data.network.StoreApi
import com.aptoide.data.repository.Response
import com.aptoide.data.repository.store.StoreRepository
import com.aptoide.data.repository.store.StoreRepositoryImpl
import com.aptoide.data.repository.store.datasource.StoreCacheDataSource
import com.aptoide.data.repository.store.datasource.StoreNetworkDataSource
import com.aptoide.data.repository.store.response.ListAppsResponse
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Test

/**
 * @author michaels on 09/03/19.
 */
class StoreRepositoryTest {

    private val storeApi = mock<StoreApi>()
    private val storeNetworkDataSource = StoreNetworkDataSource(storeApi)
    private  val storeCacheDataSource = StoreCacheDataSource()

    private val storeRepository : StoreRepository = StoreRepositoryImpl(storeNetworkDataSource, storeCacheDataSource)

    @Test
    fun testGetEditorsChoiceApps() {
        val apps = createAppList()
        val listAppsResponse = Response("ok", ListAppsResponse(apps))
        whenever(storeApi.getApps()).thenReturn(Observable.just(listAppsResponse))

        var appsResult : List<AppEntity>? = null
        val observer = storeRepository.getEditorsChoiceApplications()
        observer.subscribe { appsResult = it }

        Assert.assertEquals(apps.size, appsResult?.size)
        Assert.assertNotEquals(apps[0].name, apps[1].name)
        Assert.assertNotEquals(apps[0].packageName, apps[1].packageName)
    }

    @Test
    fun testLocalTopApps() {
        val apps = createAppList()
        val listAppsResponse = Response("ok", ListAppsResponse(apps))
        whenever(storeApi.getApps()).thenReturn(Observable.just(listAppsResponse))

        var appsResult : List<AppEntity>? = null
        val observer = storeRepository.getLocalTopApplications()
        observer.subscribe { appsResult = it }

        Assert.assertEquals(apps.size, appsResult?.size)
        Assert.assertNotEquals(apps[0].name, apps[1].name)
        Assert.assertNotEquals(apps[0].packageName, apps[1].packageName)
    }

    private fun createAppList(): List<AppEntity> {
        val app1 = AppEntity(
            "1",
            "app 1",
            "com.app.one",
            "100",
            "test store",
            "1.0",
            1,
            100,
            10,
            null,
            null,
            4.0,
            "",
            ""
        )

        val app2 = AppEntity(
            "2",
            "app 2",
            "com.app.two",
            "200",
            "test store",
            "1.5",
            2,
            120,
            11,
            null,
            null,
            4.2,
            "",
            ""
        )

        return listOf(app1, app2)
    }
}