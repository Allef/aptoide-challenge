package com.aptoide.data.repository

import com.google.gson.annotations.SerializedName

/**
 * @author michaels on 09/03/19.
 */
data class Response<T>(@SerializedName("status") val status: String,
                                @SerializedName("responses") val responses: T)