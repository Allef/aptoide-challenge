package com.aptoide.data.repository.store.response

import com.aptoide.data.entity.AppEntity
import com.google.gson.annotations.SerializedName

/**
 * @author michaels on 09/03/19.
 */
data class ListAppsResponse(@SerializedName("list") val apps : List<AppEntity>)