package com.aptoide.data.repository.store

import com.aptoide.data.entity.AppEntity
import com.aptoide.data.repository.store.datasource.StoreCacheDataSource
import com.aptoide.data.repository.store.datasource.StoreNetworkDataSource
import io.reactivex.Observable

/**
 * @author michaels on 09/03/19.
 *
 * TODO: We can use the cache datasource here to not use the service
 */
class StoreRepositoryImpl(private val networkDataSource : StoreNetworkDataSource,
                          private val cacheDataSource : StoreCacheDataSource) : StoreRepository {

    override fun getEditorsChoiceApplications(): Observable<List<AppEntity>> {
        return networkDataSource.getEditorsChoiceApplications()
    }

    override fun getLocalTopApplications(): Observable<List<AppEntity>> {
        return networkDataSource.getLocalTopApplications()
    }
}