package com.aptoide.data.repository.store.datasource

import com.aptoide.data.entity.AppEntity
import com.aptoide.data.repository.store.StoreRepository
import io.reactivex.Observable

/**
 * @author michaels on 09/03/19.
 */
class StoreCacheDataSource : StoreRepository {

    override fun getEditorsChoiceApplications(): Observable<List<AppEntity>> {
        return Observable.empty()
    }

    override fun getLocalTopApplications(): Observable<List<AppEntity>> {
        return Observable.empty()
    }
}