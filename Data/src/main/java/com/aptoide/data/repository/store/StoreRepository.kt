package com.aptoide.data.repository.store

import com.aptoide.data.entity.AppEntity
import io.reactivex.Observable

/**
 * @author michaels on 08/03/19.
 */
interface StoreRepository {

    fun getEditorsChoiceApplications() : Observable<List<AppEntity>>

    fun getLocalTopApplications() : Observable<List<AppEntity>>
}