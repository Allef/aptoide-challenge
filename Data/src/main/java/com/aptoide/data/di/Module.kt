package com.aptoide.data.di

import com.aptoide.data.BuildConfig
import com.aptoide.data.converter.DateDeserializer
import com.aptoide.data.converter.StoreAppDeserializer
import com.aptoide.data.network.StoreApi
import com.aptoide.data.repository.store.StoreRepository
import com.aptoide.data.repository.store.datasource.StoreCacheDataSource
import com.aptoide.data.repository.store.datasource.StoreNetworkDataSource
import com.aptoide.data.repository.store.response.ListAppsResponse
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * @author michaels on 09/03/19.
 */

val dataModule = module {
    single { createGson() }
    single { createOkHttpClient() }
    single { createApi<StoreApi>(get(), get(), BuildConfig.BASE_URL) }

    single { StoreNetworkDataSource(get()) }
    single { StoreCacheDataSource() }
}

internal fun createGson(): Gson {
    return GsonBuilder()
        .registerTypeAdapter(Date::class.java, DateDeserializer())
        .registerTypeAdapter(ListAppsResponse::class.java, StoreAppDeserializer())
        .create()
}

internal fun createOkHttpClient() : OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC

    return OkHttpClient.Builder()
        .connectTimeout(10L, TimeUnit.SECONDS)
        .readTimeout(10L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()
}

inline fun <reified T> createApi(okHttpClient: OkHttpClient, gson: Gson, url: String): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    return retrofit.create(T::class.java)
}