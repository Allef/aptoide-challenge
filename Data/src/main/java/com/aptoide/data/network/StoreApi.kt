package com.aptoide.data.network

import com.aptoide.data.repository.Response
import com.aptoide.data.repository.store.response.ListAppsResponse
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * @author michaels on 09/03/19.
 */
interface StoreApi {

    @GET("listApps")
    fun getApps() : Observable<Response<ListAppsResponse>>
}