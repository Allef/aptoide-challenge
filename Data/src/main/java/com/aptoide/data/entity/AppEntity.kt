package com.aptoide.data.entity

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author michaels on 09/03/19.
 */
data class AppEntity(@SerializedName("id") val id : String,
                     @SerializedName("name") val name : String,
                     @SerializedName("package") val packageName : String,
                     @SerializedName("store_id") val storeId : String,
                     @SerializedName("store_name") val storeName : String,
                     @SerializedName("vername") val versionName : String,
                     @SerializedName("vercode") val versionCode : Long,
                     @SerializedName("size") val size : Long,
                     @SerializedName("downloads") val downloads : Long,
                     @SerializedName("added") val added : Date?,
                     @SerializedName("updated") val updated : Date?,
                     @SerializedName("rating") val rating : Double,
                     @SerializedName("icon") val iconURL : String,
                     @SerializedName("graphic") val graphicURL : String?)