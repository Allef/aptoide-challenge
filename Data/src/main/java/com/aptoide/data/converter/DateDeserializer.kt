package com.aptoide.data.converter

import android.util.Log
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author michaels on 09/03/19.
 */
class DateDeserializer : JsonDeserializer<Date?> {

    override fun deserialize(json: JsonElement,
                             typeOfT: Type,
                             context: JsonDeserializationContext) : Date? {
        val date = json.asString
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

        return try {
            formatter.parse(date)
        } catch (e: ParseException) {
            Log.e(DateDeserializer::class.java.simpleName, "Failed to parse date", e)
            null
        }
    }
}