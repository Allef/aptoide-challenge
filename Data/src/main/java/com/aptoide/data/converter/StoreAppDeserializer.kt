package com.aptoide.data.converter

import com.aptoide.data.repository.store.response.ListAppsResponse
import com.google.gson.*
import java.lang.reflect.Type
import java.util.*

/**
 * @author michaels on 09/03/19.
 */
class StoreAppDeserializer : JsonDeserializer<ListAppsResponse> {

    override fun deserialize(json: JsonElement,
                             typeOfT: Type,
                             context: JsonDeserializationContext) : ListAppsResponse {
        val listApps = json.asJsonObject?.get("listApps")
        val dataSets = listApps?.asJsonObject?.get("datasets")
        val all = dataSets?.asJsonObject?.get("all")
        val data = all?.asJsonObject?.get("data")

        return GsonBuilder()
            .registerTypeAdapter(Date::class.java, DateDeserializer())
            .create()
            .fromJson(data, ListAppsResponse::class.java)
    }
}