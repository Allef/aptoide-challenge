package com.aptoide

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.aptoide.ui.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * @author michaels on 09/03/19.
 *
 * // TODO: Create more methods and steps to check layout creation in Home
 */
@RunWith(AndroidJUnit4::class)
class HomeInstrumentedTest {

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testEditorsChoiceApps() {
        onView(ViewMatchers.withId(R.id.tvLabelEditorsChoice))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}