package com.aptoide

import com.aptoide.data.di.dataModule
import com.aptoide.di.presentationModule
import com.aptoide.domain.di.domainModule
import com.aptoide.domain.interactor.usecase.GetEditorsChoiceAppsUseCase
import com.aptoide.domain.interactor.usecase.GetLocalTopAppsUseCase
import com.aptoide.ui.home.HomeViewModel
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.dsl.koinApplication
import org.koin.test.AutoCloseKoinTest
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.mock.declareMock

/**
 * @author michaels on 09/03/19.
 *
 * TODO: Create more test methods to check viewmodel and view
 */
class HomeTest : AutoCloseKoinTest(), KoinTest {

    private val getEditorsChoiceAppsUseCase = mock<GetEditorsChoiceAppsUseCase>()
    private val getLocalTopAppsUseCase = mock<GetLocalTopAppsUseCase>()

    private val viewModel: HomeViewModel by inject()

    @Before
    fun before() {
        startKoin(koinApplication {
            modules(presentationModule, domainModule, dataModule)
        })

        declareMock<GetEditorsChoiceAppsUseCase> { getEditorsChoiceAppsUseCase }
        declareMock<GetLocalTopAppsUseCase> { getLocalTopAppsUseCase }
    }

    @Test
    fun testHomeEditorsChoice() {
        // TODO: Create valid asserts to validate data
        viewModel.getEditorsChoiceApps()
            .hasObservers()
    }
}