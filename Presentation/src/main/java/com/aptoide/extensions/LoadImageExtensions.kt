package com.aptoide.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.signature.ObjectKey

/**
 * @author michaels on 09/03/19.
 */
fun ImageView.loadBitmap(url: String) {
    Glide.with(context)
        .load(url)
        // Signature changes every 5 minutes
        .signature(ObjectKey(System.currentTimeMillis()  / (5 * 60 * 1000)))
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .into(this)
}