package com.aptoide.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aptoide.domain.model.App
import com.aptoide.R
import com.aptoide.extensions.loadBitmap
import kotlinx.android.synthetic.main.item_editors_choice.view.*

/**
 * @author michaels on 09/03/19.
 */
class EditorsChoiceAppsAdapter(private val context : Context,
                               private val apps : List<App>) : RecyclerView.Adapter<EditorsChoiceAppsAdapter.ViewHolder>() {

    var onEditorsChoiceListener: OnEditorsChoiceListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.item_editors_choice, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(apps[position])
    }

    override fun getItemCount(): Int = apps.count()

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        fun bindView(app: App) {
            app.graphicURL?.let {  url ->
                itemView.ivGraphic.loadBitmap(url)
            }

            itemView.tvAppName.text = app.name
            itemView.tvAppRating.text = app.rating.toString()

            itemView.setOnClickListener { onClickListener(apps[layoutPosition]) }
        }

        private fun onClickListener(app: App) {
            onEditorsChoiceListener?.onEditorsChoiceAppSelected(app)
        }
    }

    interface OnEditorsChoiceListener {
        fun onEditorsChoiceAppSelected(app: App)
    }
}