package com.aptoide.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.aptoide.domain.model.App
import com.aptoide.R
import com.aptoide.ui.home.adapter.CategorieAppsAdapter
import com.aptoide.ui.home.adapter.EditorsChoiceAppsAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.viewModel

/**
 * @author michaels on 09/03/19.
 */
class HomeFragment : Fragment(), CategorieAppsAdapter.OnCategorieAppListener,
    EditorsChoiceAppsAdapter.OnEditorsChoiceListener {

    private val homeViewModel: HomeViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel.getEditorsChoiceApps().observe(this, Observer {
            updateEditorsChoiceApps(it)
        })

        homeViewModel.getLocalTopApps().observe(this, Observer {
            updateLocalTopApps(it)
        })

        btEditorsChoiceMore.setOnClickListener {
            Toast.makeText(context, "Show all Editors Choice Apps", Toast.LENGTH_SHORT).show()
        }

        btLocalTopMore.setOnClickListener {
            Toast.makeText(context, "Show all Local Top Apps", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateEditorsChoiceApps(apps: List<App>) {
        rvEditorsChoice.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(rvEditorsChoice)

        val adapter = EditorsChoiceAppsAdapter(context!!, apps)
        adapter.onEditorsChoiceListener = this
        rvEditorsChoice.adapter = adapter
    }

    private fun updateLocalTopApps(apps: List<App>) {
        rvLocalTop.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(rvLocalTop)

        val adapter = CategorieAppsAdapter(context!!, apps)
        adapter.onCategorieAppListener = this
        rvLocalTop.adapter = adapter
    }

    override fun onCategorieAppSelected(app: App) {
        Toast.makeText(context, "App click -> " + app.name, Toast.LENGTH_SHORT).show()
    }

    override fun onEditorsChoiceAppSelected(app: App) {
        Toast.makeText(context, "App click -> " + app.name, Toast.LENGTH_SHORT).show()
    }
}