package com.aptoide.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aptoide.domain.model.App
import com.aptoide.R
import com.aptoide.extensions.loadBitmap
import kotlinx.android.synthetic.main.item_app_categories.view.*

/**
 * @author michaels on 09/03/19.
 */
class CategorieAppsAdapter(private val context : Context,
                           private val apps : List<App>) : RecyclerView.Adapter<CategorieAppsAdapter.ViewHolder>() {

    var onCategorieAppListener: OnCategorieAppListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.item_app_categories, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(apps[position])
    }

    override fun getItemCount(): Int = apps.count()

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        fun bindView(app: App) {
            itemView.ivIcon.loadBitmap(app.iconURL)
            itemView.tvAppName.text = app.name
            itemView.tvAppRating.text = app.rating.toString()

            itemView.setOnClickListener { onClickListener(apps[layoutPosition]) }
        }

        private fun onClickListener(app: App) {
            onCategorieAppListener?.onCategorieAppSelected(app)
        }
    }

    interface OnCategorieAppListener {
        fun onCategorieAppSelected(app: App)
    }
}