package com.aptoide.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aptoide.domain.interactor.usecase.GetEditorsChoiceAppsUseCase
import com.aptoide.domain.interactor.usecase.GetLocalTopAppsUseCase
import com.aptoide.domain.model.App
import io.reactivex.observers.DisposableObserver

/**
 * @author michaels on 09/03/19.
 */
class HomeViewModel(private val getEditorsChoiceAppsUseCase: GetEditorsChoiceAppsUseCase,
                    private val getLocalTopAppsUseCase: GetLocalTopAppsUseCase) : ViewModel() {

    private val editorsChoiceApps: MutableLiveData<List<App>> by lazy {
        MutableLiveData<List<App>>().also {
            loadEditorsChoiceApps()
        }
    }

    private val localTopApps: MutableLiveData<List<App>> by lazy {
        MutableLiveData<List<App>>().also {
            loadLocalTopApps()
        }
    }

    fun getEditorsChoiceApps() : LiveData<List<App>> = editorsChoiceApps
    fun getLocalTopApps() : LiveData<List<App>> = localTopApps

    private fun loadEditorsChoiceApps() {
        getEditorsChoiceAppsUseCase.execute(object : DisposableObserver<List<App>>() {
            override fun onComplete() {
                Log.d("HomeViewModel", "Get Editors Choice -> Complete")
            }

            override fun onNext(apps: List<App>) {
                editorsChoiceApps.value = apps
            }

            override fun onError(e: Throwable) {
                Log.e("HomeViewModel", "Get Editors Choice -> Error", e)
                // TODO: Implement error flow
            }
        })
    }

    private fun loadLocalTopApps() {
        getLocalTopAppsUseCase.execute(object : DisposableObserver<List<App>>() {
            override fun onComplete() {
                Log.d("HomeViewModel", "Get Local Top -> Complete")
            }

            override fun onNext(apps: List<App>) {
                localTopApps.value = apps
            }

            override fun onError(e: Throwable) {
                Log.e("HomeViewModel", "Get Local Top -> Error", e)
                // TODO: Implement error flow
            }
        })
    }
}