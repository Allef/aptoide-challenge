package com.aptoide.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.aptoide.R
import kotlinx.android.synthetic.main.fragment_empty.view.*

/**
 * @author michaels on 09/03/19.
 */
class EmptyFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_empty, container, false)

        if(arguments?.containsKey(EXTRA_FEATURE_NAME) == true) {
            view.tvFeatureName.text = arguments?.getString(EXTRA_FEATURE_NAME)
        }

        return view
    }

    companion object {
        private const val EXTRA_FEATURE_NAME = "feature_name"

        fun newFragment(featureName: String) : EmptyFragment {
            val fragment = EmptyFragment()
            val bundle = Bundle()
            bundle.putString(EXTRA_FEATURE_NAME, featureName)
            fragment.arguments = bundle
            return fragment
        }
    }
}