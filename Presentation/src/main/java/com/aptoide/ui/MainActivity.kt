package com.aptoide.ui

import android.os.Bundle
import android.view.Menu
import androidx.fragment.app.Fragment
import com.aptoide.R
import com.aptoide.ui.home.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupBottomNavigation()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    private fun changeFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container_content, fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    private fun setupBottomNavigation() {
        bottomNavigationView.setOnNavigationItemSelectedListener {  item ->
            when(item.itemId) {
                R.id.navigation_home -> {
                    changeFragment(HomeFragment())
                    return@setOnNavigationItemSelectedListener false
                }
                R.id.navigation_search -> {
                    changeFragment(EmptyFragment.newFragment(getString(R.string.title_navigation_search)))
                    return@setOnNavigationItemSelectedListener false
                }
                R.id.navigation_stores -> {
                    changeFragment(EmptyFragment.newFragment(getString(R.string.title_navigation_stores)))
                    return@setOnNavigationItemSelectedListener false
                }
                R.id.navigation_apps -> {
                    changeFragment(EmptyFragment.newFragment(getString(R.string.title_navigation_apps)))
                    return@setOnNavigationItemSelectedListener false
                }
                else -> false
            }
        }

        bottomNavigationView.selectedItemId = R.id.navigation_home
    }
}
