package com.aptoide.di

import com.aptoide.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * @author michaels on 09/03/19.
 */
val presentationModule = module {
    viewModel { HomeViewModel(get(), get()) }
}

