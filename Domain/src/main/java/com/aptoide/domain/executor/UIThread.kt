package com.aptoide.domain.executor

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * @author michaels on 09/03/19.
 *
 * This class provides main UI thread.
 */
class UIThread  : PostExecutionThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}