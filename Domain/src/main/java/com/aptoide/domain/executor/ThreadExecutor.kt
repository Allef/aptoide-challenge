package com.aptoide.domain.executor

import java.util.concurrent.Executor

/**
 * @author michaels on 09/03/19.
 *
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the [usecase] out of the UI thread.
 */
interface ThreadExecutor : Executor