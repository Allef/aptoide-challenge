package com.aptoide.domain.di

import com.aptoide.domain.executor.PostExecutionThread
import com.aptoide.domain.executor.ThreadExecutor
import com.aptoide.domain.interactor.usecase.GetEditorsChoiceAppsUseCase
import com.aptoide.data.repository.store.StoreRepository
import com.aptoide.data.repository.store.StoreRepositoryImpl
import com.aptoide.domain.executor.JobExecutor
import com.aptoide.domain.executor.UIThread
import com.aptoide.domain.interactor.usecase.GetLocalTopAppsUseCase
import org.koin.dsl.module

/**
 * @author michaels on 09/03/19.
 */
val domainModule = module {
    single<ThreadExecutor> { JobExecutor() }
    single<PostExecutionThread> { UIThread() }

    single<StoreRepository> { StoreRepositoryImpl(get(), get()) }

    factory { GetEditorsChoiceAppsUseCase(get(), get(), get()) }
    factory { GetLocalTopAppsUseCase(get(), get(), get()) }
}