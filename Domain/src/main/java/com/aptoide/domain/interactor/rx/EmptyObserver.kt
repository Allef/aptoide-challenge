package com.aptoide.domain.interactor.rx

import io.reactivex.observers.DisposableObserver

/**
 * @author michaels on 09/03/19.
 */
open class EmptyObserver<T> : DisposableObserver<T>() {

    override fun onNext(t: T) = Unit
    override fun onError(e: Throwable) = Unit
    override fun onComplete() {}
}