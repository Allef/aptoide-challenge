package com.aptoide.domain.interactor.usecase

import com.aptoide.data.entity.AppEntity
import com.aptoide.domain.interactor.base.Mapper
import com.aptoide.domain.model.App

/**
 * @author michaels on 09/03/19.
 */
class AppEntityMapper : Mapper<AppEntity, App> {

    override fun mapFromEntity(type: AppEntity): App {
        return App(
            type.id,
            type.name,
            type.packageName,
            type.storeId,
            type.storeName,
            type.versionName,
            type.versionCode,
            type.size,
            type.downloads,
            type.added,
            type.updated,
            type.rating,
            type.iconURL,
            type.graphicURL
        )
    }

    override fun mapToEntity(type: App): AppEntity {
        return AppEntity(
            type.id,
            type.name,
            type.packageName,
            type.storeId,
            type.storeName,
            type.versionName,
            type.versionCode,
            type.size,
            type.downloads,
            type.added,
            type.updated,
            type.rating,
            type.iconURL,
            type.graphicURL
        )
    }
}