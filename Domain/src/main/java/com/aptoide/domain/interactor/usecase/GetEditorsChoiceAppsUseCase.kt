package com.aptoide.domain.interactor.usecase

import com.aptoide.data.repository.store.StoreRepository
import com.aptoide.domain.executor.PostExecutionThread
import com.aptoide.domain.executor.ThreadExecutor
import com.aptoide.domain.interactor.base.ObservableUseCase
import com.aptoide.domain.model.App
import io.reactivex.Observable

/**
 * @author michaels on 09/03/19.
 */
class GetEditorsChoiceAppsUseCase(threadExecutor: ThreadExecutor,
                                  postExecutionThread: PostExecutionThread,
                                  private val storeRepository: StoreRepository)
    : ObservableUseCase<List<App>, Unit>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Unit?): Observable<List<App>> {
        return storeRepository.getEditorsChoiceApplications()
            .map { it.filter { appEntity -> appEntity.graphicURL?.isNotEmpty() == true } }
            .map { it.map { entity -> AppEntityMapper().mapFromEntity(entity) } }

    }
}