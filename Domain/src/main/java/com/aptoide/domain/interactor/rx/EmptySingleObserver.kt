package com.aptoide.domain.interactor.rx

import io.reactivex.observers.DisposableSingleObserver

/**
 * @author michaels on 09/03/19.
 */
open class EmptySingleObserver<T> : DisposableSingleObserver<T>() {

    override fun onSuccess(result: T) = Unit
    override fun onError(throwable: Throwable) = Unit
}