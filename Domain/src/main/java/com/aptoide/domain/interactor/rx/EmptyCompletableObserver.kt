package com.aptoide.domain.interactor.rx

import io.reactivex.observers.DisposableCompletableObserver

/**
 * @author michaels on 09/03/19.
 */
open class EmptyCompletableObserver : DisposableCompletableObserver() {

    override fun onComplete() = Unit
    override fun onError(e: Throwable) = Unit
}