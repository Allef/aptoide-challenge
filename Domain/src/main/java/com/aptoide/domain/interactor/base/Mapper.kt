package com.aptoide.domain.interactor.base

/**
 * @author michaels on 09/03/19.
 */
interface Mapper<E, D> : EntityToMapMapper<E, D>, MapToEntityMapper<E, D>

interface EntityToMapMapper<E, D> {
    fun mapFromEntity(type: E): D
}

interface MapToEntityMapper<E, D> {
    fun mapToEntity(type: D): E
}