package com.aptoide.domain.model

import java.util.*

/**
 * @author michaels on 08/03/19.
 */
data class App(val id : String,
               val name : String,
               val packageName : String,
               val storeId : String,
               val storeName : String,
               val versionName : String,
               val versionCode : Long,
               val size : Long,
               val downloads : Long,
               val added : Date?,
               val updated : Date?,
               val rating : Double,
               val iconURL : String,
               val graphicURL : String?)