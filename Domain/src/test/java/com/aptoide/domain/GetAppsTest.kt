package com.aptoide.domain

import com.aptoide.data.entity.AppEntity
import com.aptoide.data.repository.store.StoreRepository
import com.aptoide.domain.executor.PostExecutionThread
import com.aptoide.domain.executor.ThreadExecutor
import com.aptoide.domain.interactor.usecase.GetEditorsChoiceAppsUseCase
import com.aptoide.domain.interactor.usecase.GetLocalTopAppsUseCase
import com.aptoide.domain.model.App
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito

/**
 * @author michaels on 09/03/19.
 */
class GetAppsTest {

    private val threadExecutorMock = mock<ThreadExecutor> {}
    private val postExecutionThreadMock = mock<PostExecutionThread> {
        on { scheduler }.thenReturn(Schedulers.trampoline())
    }

    private val storeRepositoryMock = mock<StoreRepository>()

    private val getEditorsChoiceAppsUseCase = GetEditorsChoiceAppsUseCase(threadExecutorMock, postExecutionThreadMock, storeRepositoryMock)
    private val getLocalTopAppsUseCaseAppsUseCase = GetLocalTopAppsUseCase(threadExecutorMock, postExecutionThreadMock, storeRepositoryMock)

    @Test
    fun testGetEditorsChoiceAppUseCase() {
        val apps = createEditorsChoiceAppList()
        val appEntities = createAppEntityList()
        whenever(storeRepositoryMock.getEditorsChoiceApplications()).thenReturn(Observable.just(appEntities))

        val observer = getEditorsChoiceAppsUseCase.buildUseCaseObservable().test()

        Mockito.verify(storeRepositoryMock).getEditorsChoiceApplications()
        Assert.assertNotNull(observer)
        observer.assertValue(apps)
    }

    @Test
    fun testLocalTopAppUseCase() {
        val apps = createLocalTopAppList()
        val appEntities = createAppEntityList()
        whenever(storeRepositoryMock.getLocalTopApplications()).thenReturn(Observable.just(appEntities))

        val observer = getLocalTopAppsUseCaseAppsUseCase.buildUseCaseObservable().test()

        Mockito.verify(storeRepositoryMock).getLocalTopApplications()
        Assert.assertNotNull(observer)
        observer.assertValue(apps)
    }

    private fun createEditorsChoiceAppList(): List<App> {
        val app1 = App(
            "1",
            "app 1",
            "com.app.one",
            "100",
            "test store",
            "1.0",
            1,
            100,
            10,
            null,
            null,
            4.0,
            "",
            "www.aptoide.com/image/app1"
        )

        return listOf(app1)
    }

    private fun createLocalTopAppList(): List<App> {
        val app1 = App(
            "1",
            "app 1",
            "com.app.one",
            "100",
            "test store",
            "1.0",
            1,
            100,
            10,
            null,
            null,
            4.0,
            "",
            "www.aptoide.com/image/app1"
        )

        val app2 = App(
            "2",
            "app 2",
            "com.app.two",
            "200",
            "test store",
            "1.5",
            2,
            120,
            11,
            null,
            null,
            4.2,
            "",
            ""
        )

        return listOf(app1, app2)
    }

    private fun createAppEntityList(): List<AppEntity> {
        val app1 = AppEntity(
            "1",
            "app 1",
            "com.app.one",
            "100",
            "test store",
            "1.0",
            1,
            100,
            10,
            null,
            null,
            4.0,
            "",
            "www.aptoide.com/image/app1"
        )

        val app2 = AppEntity(
            "2",
            "app 2",
            "com.app.two",
            "200",
            "test store",
            "1.5",
            2,
            120,
            11,
            null,
            null,
            4.2,
            "",
            ""
        )

        return listOf(app1, app2)
    }
}